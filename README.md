# File Management API

Este proyecto implementa una API para la gestión de archivos, utilizando Java 17, Spring Boot 3, PostgreSQL como base de datos y un contenedor Docker para facilitar el despliegue.

## Endpoints

### 1. Cargar Lista de Archivos con Hash SHA-256 y SHA-512

**Descripción:**
Este endpoint permite cargar una lista de archivos con sus respectivos hashes SHA-256 y SHA-512. Si se carga el mismo archivo con el mismo hash, la fecha de carga se actualizará.

**URL:**


POST /api/documents/hash




### 2. Visualizar Todos los Registros de Archivos Cargados

**Descripción:**
Este endpoint muestra todos los registros de archivos cargados en la base de datos.

**URL:**


GET /api/documents




### 3. Mostrar un Registro de Archivo por Hash y Tipo de Hash

**Descripción:**
Este endpoint muestra un registro de archivo recibiendo como parámetros un hash y un tipo de hash (SHA-256 o SHA-512).

**URL:**

GET /api/documents?hash={hash}&hashType={hashType}



## Tecnologías Utilizadas

- Java 17
- Spring Boot 3
- PostgreSQL
- Docker





