package com.test.files.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO (Data Transfer Object) que representa la información básica de un
 * registro de archivo.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileRecordDto {

	/**
	 * Nombre del archivo.
	 */
	private String fileName;

	/**
	 * Valor hash del archivo.
	 */
	private String hash;

	/**
	 * Fecha y hora de la última carga del archivo.
	 */
	private Date lastUpload;
}
