package com.test.files.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class FileResponse {

	@Getter
	@Setter
	private String algorithm;

	private List<FileRecord> fileRecord;

}
