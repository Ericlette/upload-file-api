package com.test.files.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "fileName", "hashSHA256", "hashSHA512" })
@ToString
@Getter
@Setter
@Table(name = "file_record")
public class FileRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "fileName", nullable = false)
	private String fileName;

	@Column(name = "hash-sha-256")
	private String hashSHA256;

	@Column(name = "hash-sha-512")
	private String hashSHA512;

	@Column(name = "lastUpload")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpload;

}
