package com.test.files.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Representa una respuesta de error que se devuelve en caso de fallo en una
 * solicitud.
 */
@Getter
@Setter
public class ErrorResponse {

	/**
	 * Marca de tiempo que indica cuándo ocurrió el error.
	 */
	private final long timestamp;

	/**
	 * Código de estado HTTP que indica el tipo de error.
	 */
	private final int status;

	/**
	 * Mensaje descriptivo del error.
	 */
	private final String message;

	/**
	 * Ruta de la solicitud que generó el error.
	 */
	private final String path;

	/**
	 * Constructor para inicializar una instancia de ErrorResponse.
	 *
	 * @param timestamp Marca de tiempo que indica cuándo ocurrió el error.
	 * @param status    Código de estado HTTP que indica el tipo de error.
	 * @param message   Mensaje descriptivo del error.
	 * @param path      Ruta de la solicitud que generó el error.
	 */
	public ErrorResponse(long timestamp, int status, String message, String path) {
		if (timestamp < 0) {
			throw new IllegalArgumentException("El timestamp no puede ser negativo");
		}
		this.timestamp = timestamp;
		this.status = status;
		this.message = message;
		this.path = path;
	}
}
