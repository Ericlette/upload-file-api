package com.test.files.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Excepción personalizada para representar errores específicos de la
 * aplicación.
 */
@Getter
@Setter
public class MyAppException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Código de estado HTTP que indica el tipo de error.
	 */
	private final int status;

	/**
	 * Mensaje descriptivo del error.
	 */
	private final String message;

	/**
	 * Ruta de la solicitud que generó el error.
	 */
	private final String path;

	/**
	 * Marca de tiempo que indica cuándo ocurrió el error.
	 */
	private final long timestamp;

	/**
	 * Constructor para inicializar una instancia de MyAppException.
	 *
	 * @param status  Código de estado HTTP que indica el tipo de error.
	 * @param message Mensaje descriptivo del error.
	 * @param path    Ruta de la solicitud que generó el error.
	 */
	public MyAppException(int status, String message, String path) {
		super(message);
		this.status = status;
		this.message = message;
		this.path = path;
		this.timestamp = System.currentTimeMillis();
	}
}
