package com.test.files.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.test.files.dto.FileRecordDto;
import com.test.files.exception.MyAppException;
import com.test.files.model.FileRecord;
import com.test.files.model.FileResponse;

public interface IntegrationService {

	FileResponse processDocument(MultipartFile[] documents, String algorithm) throws MyAppException;

	FileRecordDto findByHashAndType(String hash, String typeHash);

	List<FileRecord> listAll();

}
