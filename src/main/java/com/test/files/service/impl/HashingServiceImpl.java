package com.test.files.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.files.exception.MyAppException;
import com.test.files.service.HashingService;

/**
 * Implementación del servicio de hashing que proporciona funciones para calcular
 * el hash de un documento utilizando algoritmos como SHA-256 o SHA-512.
 */
@Service
public class HashingServiceImpl implements HashingService {

    /**
     * Calcula el hash de un documento utilizando el algoritmo especificado.
     *
     * @param document  Bytes del documento.
     * @param fileName  Nombre del archivo.
     * @param typeHash  Algoritmo de hash a utilizar (SHA-256 o SHA-512).
     * @return Hash calculado en formato hexadecimal.
     * @throws MyAppException Si ocurre un error durante el cálculo del hash.
     */
    @Override
    public String calculateHash(byte[] document, String fileName, String typeHash) {
        try {
            // Validación del algoritmo de hash
            validateHashAlgorithm(typeHash);

            // Combinar bytes del documento y nombre del archivo
            byte[] combinedBytes = combineBytesAndFileName(document, fileName);

            // Cálculo del hash utilizando el algoritmo especificado
            MessageDigest digest = MessageDigest.getInstance(typeHash);
            byte[] hash = digest.digest(combinedBytes);

            // Conversión del hash a formato hexadecimal
            return bytesToHex(hash);
        } catch (NoSuchAlgorithmException e) {
            // Error si el algoritmo de hash no es admitido
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Algoritmo de hash no admitido: " + typeHash, "/api/documents/hash");
        }
    }

    /**
     * Combina los bytes del documento y el nombre del archivo en un solo array de bytes.
     *
     * @param document Bytes del documento.
     * @param fileName Nombre del archivo.
     * @return Array de bytes combinado.
     */
    private byte[] combineBytesAndFileName(byte[] document, String fileName) {
        byte[] fileNameBytes = fileName.getBytes();
        byte[] combinedBytes = new byte[document.length + fileNameBytes.length];
        System.arraycopy(document, 0, combinedBytes, 0, document.length);
        System.arraycopy(fileNameBytes, 0, combinedBytes, document.length, fileNameBytes.length);
        return combinedBytes;
    }

    /**
     * Valida si el algoritmo de hash especificado es admitido.
     *
     * @param typeHash Algoritmo de hash a validar.
     * @throws MyAppException Si el algoritmo de hash no es admitido.
     */
    private void validateHashAlgorithm(String typeHash) {
        if (!"SHA-256".equals(typeHash) && !"SHA-512".equals(typeHash)) {
            // Error si el tipo de hash no es admitido
            throw new MyAppException(HttpStatus.BAD_REQUEST.value(),
                    "El parámetro ‘hash’ solo puede ser ‘SHA-256’ o ‘SHA-512’", "/api/documents/hash");
        }
    }

    /**
     * Convierte un array de bytes a su representación en formato hexadecimal.
     *
     * @param hash Array de bytes del hash.
     * @return Representación en formato hexadecimal.
     */
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexStringBuilder = new StringBuilder();
        for (byte b : hash) {
            hexStringBuilder.append(String.format("%02x", b));
        }
        return hexStringBuilder.toString();
    }
}
