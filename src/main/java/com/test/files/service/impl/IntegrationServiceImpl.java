package com.test.files.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.test.files.dto.FileRecordDto;
import com.test.files.exception.MyAppException;
import com.test.files.model.FileRecord;
import com.test.files.model.FileResponse;
import com.test.files.service.FileRecordService;
import com.test.files.service.HashingService;
import com.test.files.service.IntegrationService;

/**
 * Implementación del servicio de integración que procesa documentos, realiza
 * hashing y proporciona operaciones relacionadas con los registros de archivos.
 */
@Service
public class IntegrationServiceImpl implements IntegrationService {

	@Autowired
	private FileRecordService fileRecordService;

	@Autowired
	private HashingService hashingService;

	/**
	 * Procesa los documentos recibidos, realiza el hashing y guarda o actualiza los
	 * registros de archivos en la base de datos.
	 *
	 * @param documents Lista de documentos a procesar.
	 * @param algorithm Algoritmo de hashing a utilizar (SHA-256 o SHA-512).
	 * @return Respuesta que contiene el algoritmo utilizado y los registros de
	 *         archivos procesados.
	 * @throws MyAppException Si ocurre un error durante el procesamiento.
	 */
	@Override
	@Transactional
	public FileResponse processDocument(MultipartFile[] documents, String algorithm) {
		try {
			if (!"SHA-256".equals(algorithm) && !"SHA-512".equals(algorithm)) {
				throw new MyAppException(HttpStatus.BAD_REQUEST.value(),
						"El parámetro ‘hashType’ solo puede ser ‘SHA-256’ o ‘SHA-512’", "/api/documents/hash");
			}

			Set<FileRecord> listFile = new HashSet<>();

			// Procesamiento de cada documento
			for (MultipartFile document : documents) {

				String fileName = document.getOriginalFilename();

				String hash = hashingService.calculateHash(document.getBytes(), fileName, algorithm);
				FileRecord fileRecord = fileRecordService.getFileRecordByFileName(fileName);

				if (fileRecord == null) {
					fileRecord = new FileRecord();
					fileRecord.setFileName(fileName);
					setHash(fileRecord, hash, algorithm);
				} else {
					if (hash.equals(fileRecord.getHashSHA256()) || hash.equals(fileRecord.getHashSHA512()))
						fileRecord.setLastUpload(new Date());
					else
						setHash(fileRecord, hash, algorithm);
				}

				listFile.add(fileRecord);
			}

			// Convierte el SET en un LIST y Guarda o actualiza los registros de archivos
			List<FileRecord> listFileRecords = new ArrayList<>(listFile);
			fileRecordService.saveOrUpdateFileRecords(listFileRecords);

			// Construye la respuesta
			FileResponse fileResponse = new FileResponse();
			fileResponse.setAlgorithm(algorithm);
			fileResponse.setFileRecord(listFileRecords);

			return fileResponse;

		} catch (IOException e) {
			throw new MyAppException(HttpStatus.BAD_REQUEST.value(), "Error durante el procesamiento del documento",
					"/api/documents/hash");
		} catch (MyAppException e) {
			throw e;
		} catch (Exception e) {
			throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno del servidor",
					"/api/documents/hash");
		}
	}

	/**
	 * Busca y devuelve la información del archivo por hash y tipo de hash.
	 *
	 * @param hash     Hash del archivo.
	 * @param typeHash Tipo de algoritmo de hash (SHA-256 o SHA-512).
	 * @return Información del archivo en formato DTO.
	 * @throws MyAppException Si ocurre un error durante la búsqueda.
	 */
	@Override
	public FileRecordDto findByHashAndType(String hash, String typeHash) {
		try {
			FileRecord fileRecord;

			if ("SHA-256".equals(typeHash)) {
				fileRecord = fileRecordService.getFileRecordByHashSHA256(hash);
			} else if ("SHA-512".equals(typeHash)) {
				fileRecord = fileRecordService.getFileRecordByHashSHA512(hash);
			} else {
				throw new MyAppException(HttpStatus.BAD_REQUEST.value(),
						"El parámetro ‘hashType’ solo puede ser ‘SHA-256’ o ‘SHA-512’", "/api/documents");
			}

			// Convierte el registro de archivo a DTO
			return parseFileRecord(fileRecord, typeHash);
		} catch (MyAppException e) {
			throw e;
		} catch (Exception e) {
			throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error al procesar la solicitud",
					"/api/documents");
		}
	}

	/**
	 * Lista y devuelve todos los registros de archivos almacenados.
	 *
	 * @return Lista de registros de archivos.
	 * @throws MyAppException Si ocurre un error durante la obtención.
	 */
	@Override
	public List<FileRecord> listAll() {
		try {
			// Obtiene todos los registros de archivos
			return fileRecordService.getAllFileRecords();
		} catch (Exception e) {
			throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error al obtener todos los registros",
					"/api/documents");
		}
	}

	/**
	 * Establece el hash en el registro de archivo según el algoritmo especificado.
	 *
	 * @param fileRecord Registro de archivo al que se le establecerá el hash.
	 * @param hash       Valor del hash.
	 * @param algorithm  Algoritmo de hashing utilizado (SHA-256 o SHA-512).
	 */
	private void setHash(FileRecord fileRecord, String hash, String algorithm) {
		if ("SHA-256".equals(algorithm)) {
			fileRecord.setHashSHA256(hash);
		} else {
			fileRecord.setHashSHA512(hash);
		}
	}

	/**
	 * Convierte el registro de archivo a un DTO con el tipo de hash especificado.
	 *
	 * @param fileRecord Registro de archivo a convertir.
	 * @param hashType   Tipo de hash a utilizar (SHA-256 o SHA-512).
	 * @return DTO que representa la información del archivo.
	 */
	private FileRecordDto parseFileRecord(FileRecord fileRecord, String hashType) {
		if (fileRecord != null) {
			FileRecordDto fileRecordDTO = new FileRecordDto();
			fileRecordDTO.setFileName(fileRecord.getFileName());
			fileRecordDTO.setHash("SHA-256".equals(hashType) ? fileRecord.getHashSHA256() : fileRecord.getHashSHA512());
			fileRecordDTO.setLastUpload(fileRecord.getLastUpload());
			return fileRecordDTO;
		}
		return null;
	}
}
