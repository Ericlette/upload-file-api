package com.test.files.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.files.exception.MyAppException;
import com.test.files.model.FileRecord;
import com.test.files.repository.FileRepository;
import com.test.files.service.FileRecordService;

/**
 * Implementación del servicio para operaciones relacionadas con los registros de archivos.
 */
@Service
public class FileRecordServiceImpl implements FileRecordService {

    private final FileRepository repository;

    /**
     * Constructor que inyecta la dependencia del repositorio de archivos.
     *
     * @param repository Repositorio de archivos.
     */
    public FileRecordServiceImpl(FileRepository repository) {
        this.repository = repository;
    }

    /**
     * Obtiene todos los registros de archivos.
     *
     * @return Lista de registros de archivos.
     * @throws MyAppException Si ocurre un error al obtener los registros.
     */
    @Override
    public List<FileRecord> getAllFileRecords() {
        try {
            return repository.findAll();
        } catch (Exception e) {
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Error al obtener todos los registros", "/api/documents");
        }
    }

    /**
     * Obtiene un registro de archivo por nombre de archivo.
     *
     * @param fileName Nombre del archivo.
     * @return Registro de archivo o null si no se encuentra.
     * @throws MyAppException Si ocurre un error al obtener el registro por nombre de archivo.
     */
    @Override
    public FileRecord getFileRecordByFileName(String fileName) {
        try {
            Optional<FileRecord> fileRecord = repository.findFileRecordByFileName(fileName);
            return fileRecord.orElse(null);
        } catch (Exception e) {
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Error al obtener el registro por nombre de archivo: " + fileName, "/api/documents");
        }
    }

    /**
     * Guarda o actualiza una lista de registros de archivos.
     *
     * @param listFileRecord Lista de registros de archivos a guardar o actualizar.
     * @return Lista de registros de archivos guardados o actualizados.
     * @throws MyAppException Si ocurre un error al guardar o actualizar los registros.
     */
    @Override
    public List<FileRecord> saveOrUpdateFileRecords(List<FileRecord> listFileRecord) {
        try {
            return repository.saveAll(listFileRecord);
        } catch (Exception e) {
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Error al guardar o actualizar registros", "/api/documents");
        }
    }

    /**
     * Obtiene un registro de archivo por hash SHA-256.
     *
     * @param hash Hash SHA-256.
     * @return Registro de archivo o null si no se encuentra.
     * @throws MyAppException Si ocurre un error al obtener el registro por hash SHA-256.
     */
    @Override
    public FileRecord getFileRecordByHashSHA256(String hash) {
        try {
            return repository.findByHashSHA256(hash).orElse(null);
        } catch (Exception e) {
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Error al obtener el registro por hash SHA-256: " + hash, "/api/documents");
        }
    }

    /**
     * Obtiene un registro de archivo por hash SHA-512.
     *
     * @param hash Hash SHA-512.
     * @return Registro de archivo o null si no se encuentra.
     * @throws MyAppException Si ocurre un error al obtener el registro por hash SHA-512.
     */
    @Override
    public FileRecord getFileRecordByHashSHA512(String hash) {
        try {
            return repository.findByHashSHA512(hash).orElse(null);
        } catch (Exception e) {
            throw new MyAppException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "Error al obtener el registro por hash SHA-512: " + hash, "/api/documents");
        }
    }
}
