package com.test.files.service;

import java.util.List;
import java.util.Set;

import com.test.files.exception.MyAppException;
import com.test.files.model.FileRecord;

public interface FileRecordService {

	List<FileRecord> getAllFileRecords();

	FileRecord getFileRecordByFileName(String FileRecordName) throws MyAppException;

	FileRecord getFileRecordByHashSHA256(String hash) throws MyAppException;

	FileRecord getFileRecordByHashSHA512(String hash) throws MyAppException;

	public List<FileRecord> saveOrUpdateFileRecords(List<FileRecord> FileRecord) throws MyAppException;
}
