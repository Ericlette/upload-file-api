package com.test.files.service;

import com.test.files.exception.MyAppException;

public interface HashingService {

	String calculateHash(byte[] document, String fileName, String algorithm) throws MyAppException;

}