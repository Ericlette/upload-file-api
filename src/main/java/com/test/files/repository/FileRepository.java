package com.test.files.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.files.model.FileRecord;

import java.util.Optional;

public interface FileRepository extends JpaRepository<FileRecord, Long> {
	
	Optional<FileRecord> findFileRecordByFileName(String fileName);
	
    Optional<FileRecord> findByHashSHA256(String hashSHA256);

    Optional<FileRecord> findByHashSHA512(String hashSHA512);
}