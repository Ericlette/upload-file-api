package com.test.files.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.test.files.dto.FileRecordDto;
import com.test.files.exception.ErrorResponse;
import com.test.files.exception.MyAppException;
import com.test.files.model.FileRecord;
import com.test.files.service.IntegrationService;

/**
 * Controlador para manejar las operaciones relacionadas con archivos y
 * documentos.
 */
@RestController
@RequestMapping("/api/documents")
public class FileController {

	@Autowired
	private IntegrationService integrationService;

	/**
	 * Endpoint para cargar archivos y calcular el hash.
	 * 
	 * @param files    Arreglo de archivos a cargar.
	 * @param hashType Tipo de hash a utilizar (SHA-256 o SHA-512).
	 * @return ResponseEntity que contiene el resultado del proceso.
	 */
	@PostMapping("/hash")
	public ResponseEntity<Object> uploadFiles(@RequestParam("files") MultipartFile[] files,
			@RequestParam("hashType") String hashType) {
		try {
			return ResponseEntity.ok(integrationService.processDocument(files, hashType));
		} catch (MyAppException e) {
			ErrorResponse errorResponse = new ErrorResponse(e.getTimestamp(), e.getStatus(), e.getMessage(),
					e.getPath());
			return ResponseEntity.status(e.getStatus()).body(errorResponse);
		}
	}

	/**
	 * Endpoint para listar todos los archivos.
	 * 
	 * @return ResponseEntity que contiene la lista de archivos.
	 */
	@GetMapping()
	public ResponseEntity<Object> listAllFiles() {
		try {
			List<FileRecord> documents = integrationService.listAll();
			return ResponseEntity.ok(documents);
		} catch (MyAppException e) {
			ErrorResponse errorResponse = new ErrorResponse(e.getTimestamp(), e.getStatus(), e.getMessage(),
					e.getPath());
			return ResponseEntity.status(e.getStatus()).body(errorResponse);
		}
	}

	/**
	 * Endpoint para buscar un archivo por su hash y tipo de hash.
	 * 
	 * @param hash     Valor del hash a buscar.
	 * @param hashType Tipo de hash a utilizar (SHA-256 o SHA-512).
	 * @return ResponseEntity que contiene el documento encontrado.
	 */
	@GetMapping(params = { "hash", "hashType" })
	public ResponseEntity<Object> findByHashAndHashType(@RequestParam String hash, @RequestParam String hashType) {
		try {
			FileRecordDto document = integrationService.findByHashAndType(hash, hashType);
			return ResponseEntity.ok(document);
		} catch (MyAppException e) {
			ErrorResponse errorResponse = new ErrorResponse(e.getTimestamp(), e.getStatus(), e.getMessage(),
					e.getPath());
			return ResponseEntity.status(e.getStatus()).body(errorResponse);
		}
	}
}
